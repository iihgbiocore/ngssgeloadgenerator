#!/bin/bash

TEST_DIR="./"
if [ $# -gt 0 ]
then
   TEST_DIR=$1
fi

HEADER_OUT=true

rm -rf $TEST_DIR/netstat_summary.*
rm -rf $TEST_DIR/netstat_summary_sorted.*

for f in `find $TEST_DIR -type f -name "netstat_summary_[0-9]**"`
do
    echo $f
    file=`basename ${f} .out`
    arr=(${file//_/ })

#    echo ${arr[*]}
#    echo ${arr[0]}
#    echo ${arr[1]}
#    echo ${arr[2]}
    JOB_ID=${arr[2]}
    CLIENT_ID=${arr[3]}

    if $HEADER_OUT
    then
        HEADER=`head -n 1 ${f}` 
        echo 'client_id job_id '${HEADER} > ${TEST_DIR}/netstat_summary.out
        HEADER_OUT=false
    fi

    # Don't want to process the header.
    tail -n+2 ${f}|xargs -I {} echo "$CLIENT_ID $JOB_ID {}" >> ${TEST_DIR}/netstat_summary.out
     
done

# lets copy over the header.
head -n 1 ${TEST_DIR}/netstat_summary.out | tr " " ";" > ${TEST_DIR}/netstat_summary_sorted.ssv
# now, sort the file - minus header.
tail -n+2 ${TEST_DIR}/netstat_summary.out | sort -t ' ' -k 3,3 -n | tr " " ";" >>${TEST_DIR}/netstat_summary_sorted.ssv

# all done.
echo "files merged and sorted: ${TEST_DIR}/netstat_summary_sorted.out ${TEST_DIR}/netstat_summary.out"

exit
