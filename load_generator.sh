#!/bin/bash

###################################
# This script will slowly ramp up 
# jobs to the cluster.
#
# DATA_ROOT : where files should be read/written from during the test  -- this should be the suspected bottleneck
# TEST_NAME : the name of the overall test being run (way to group common results from different compute nodes)
# CLIENT_ID : the id of the client being launched
# PREFIX : this is some sub-identifier or name which is used in generating metadata files about the test run.
# RUN_ROOT : this is a root directory to consolidate all output under for all clients after the job has finished.
###################################

SOURCE_DIR=$( cd $( dirname $0 ) && pwd )
export LG_HOME=${SOURCE_DIR}

# Max clients to launch
CLIENTS=150

# Time to wait between client launches - default 10 mins.
WAIT_TIME=600

# Name of the test, this will be used to group the results from all dispatched jobs
TEST_NAME='Test'

# The prefix will be used to invoke job scripts as well as label output files
PREFIX=''

# The root directory holding the files to read/write from 
DATA_ROOT=/mnt/radon/galaxy/performance_test

# The root directory to store all results from the test run under. 
RUN_ROOT=/Users/galaxy/performance_test

# Options to use when dispatching via qsub
QSUB_OPTS="-V -ckpt user"
TEST_QSUB_OPTS="-V"
#Q_ALL="-q all.q -l ib=true"
Q_ALL="-q all.q -pe smp1 8"
Q_UI="-q UI -pe smp1 12"
Q_IIHG="-q IIHG -pe smp1 12"
Q_TEST="-q ibtest -pe smp1 8"
IIHG_C=0
UI_C=0
ALL_C=0
TEST_C=0
#QSUB_OPTS="-clear -V -q IIHG -pe smp1 12"

# Options to pass through to the jobs that are dispatched
JOB_ARGS=''

usage(){

cat << EOF
     
usage: $0 options

This script will ramp up jobs to the cluster

OPTIONS:
   -h      Show this message
   -c      Max number of jobs to launch. Default 150
   -w      Wait time (in seconds) between job launches.  Default is 600 secs (10 mins)
   -t      Name of the test, used to group the results from all dispatched jobs. Default is \'Test\'
   -p      Prefix to use in finding the job scripts as well as naming output files. 
   -d      The root directory holding the files to read/write from in the jobs.  Default is /iihg/data/performance_test.
   -q      qsub native options to use when invoking jobs.  Default is $QSUB_OPTS.
   -a      Arguments to send onto the launched jobs.
     
EOF
}

printArgs(){
cat << EOF
     
run arguments:

     CLIENTS = $CLIENTS
     WAIT_TIME = $WAIT_TIME
     TEST_NAME = $TEST_NAME
     PREFIX = $PREFIX
     DATA_ROOT = $DATA_ROOT
     QSUB_OPTS = $QSUB_OPTS
     JOB_ARGS = $JOB_ARGS
     
EOF
}

while getopts "hc:w:t:p:d:q:a:" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         c)
             CLIENTS=$OPTARG
             ;;
         w)
             WAIT_TIME=$OPTARG
             ;; 
         t)
             TEST_NAME=$OPTARG
             ;;
         p)
             PREFIX=$OPTARG
             ;;
         d)
             DATA_ROOT=$OPTARG
             ;;
         q)
             QSUB_OPTS=$OPTARG
             ;;
         a)
             JOB_ARGS=$OPTARG
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

printArgs
export DATA_ROOT 
export PREFIX 
export TEST_NAME
export RUN_ROOT

TEST_DIR=$RUN_ROOT/$TEST_NAME
if [ -d $TEST_DIR ]
then
    echo "Test directory already exists. Please rename or remove it before trying to re-launch the test."
    exit 1
fi
mkdir $TEST_DIR

# Kick off storage monitoring
for i in {0..3}
do
   ssh -tt OSS-0-${i} "~/performance_test/OSS_netstat_monitor.sh ${TEST_NAME}" < /dev/null &
   NETSTAT_PID[${i}]=$!
#   ssh -tt OSS-0-${i} "~/performance_test/dstat-0.7.2/dstat -Ttcdngyrm --output ~/performance_test/OSS-0-${i}_dstat.cvs 30" >/dev/null &
#   DSTAT_PID[${i}]=$!
   cat /dev/null > ~/performance_test/OSS-0-${i}_dstat.cvs
#   cat ~/performance_test/dstat.header > ~/performance_test/OSS-0-${i}_dstat.cvs
done
NETSTAT_PID_3=$!
for (( c=1; c<=$CLIENTS; c++))
do
    CLIENT_ID=$c
    WORKING_DIR=${TEST_DIR}/${PREFIX}-${CLIENT_ID}_cwd
    mkdir ${WORKING_DIR}
    pushd ${WORKING_DIR}
    echo "Launching client ${CLIENT_ID} ..."

    # Try various queues such that we can run as many jobs in parallel that we can
    if [ ${IIHG_C} -lt 1 ]
    then
         echo "Trying IIHG q.  Current number dispatched to IIHG q: ${IIHG_C}"
         qsub -clear -now y ${Q_IIHG} ${QSUB_OPTS} -cwd ${SOURCE_DIR}/client_job.sh $CLIENT_ID $JOB_ARGS
         if [ $? -eq 1 ]; then
             echo "$?. Trying UI q. IIHG q is busy. Current number dispatched to UI q: ${UI_C}"
             qsub -clear -now y ${Q_UI} ${QSUB_OPTS} -cwd ${SOURCE_DIR}/client_job.sh $CLIENT_ID $JOB_ARGS
             if [ $? -eq 1 ]; then
                 echo "Submitting to test.q.  UI q is busy."
                 qsub -clear -now y ${Q_TEST} ${TEST_QSUB_OPTS} -cwd ${SOURCE_DIR}/client_job.sh $CLIENT_ID $JOB_ARGS
                 if [ $? -eq 1 ]; then
                      echo "Submitting to all.q. TEST q is busy."
                      qsub -clear ${Q_ALL} ${QSUB_OPTS} -cwd ${SOURCE_DIR}/client_job.sh $CLIENT_ID $JOB_ARGS
                      ALL_C=$((ALL_C+1))
                 else 
                      TEST_C=$((TEST_C+1))
                 fi
             else
                 UI_C=$((UI_C+1))
             fi
         else
            IIHG_C=$((IIHG_C+1))
         fi
    elif [ ${UI_C} -lt 25 ]
    then
        echo "Trying UI q. Current number dispatched to UI q: ${UI_C}"
        qsub -clear -now y ${Q_UI} ${QSUB_OPTS} -cwd ${SOURCE_DIR}/client_job.sh $CLIENT_ID $JOB_ARGS
        if [ $? -eq 1 ]; then
             echo "Submitting to test.q.  UI q is busy."
             qsub -clear -now y ${Q_TEST} ${TEST_QSUB_OPTS} -cwd ${SOURCE_DIR}/client_job.sh $CLIENT_ID $JOB_ARGS
             if [ $? -eq 1 ]; then
                  echo "Submitting to all.q. TEST q is busy."
                  qsub -clear ${Q_ALL} ${QSUB_OPTS} -cwd ${SOURCE_DIR}/client_job.sh $CLIENT_ID $JOB_ARGS
                  ALL_C=$((ALL_C+1))
             else 
                  TEST_C=$((TEST_C+1))
             fi
        else
            UI_C=$((UI_C+1))
        fi
    else
         if [ ${TEST_C} -lt 33 ]
         then
             echo "Trying TEST q. Current number dispatched to TEST q: ${TEST_C}"
             qsub -clear -now y ${Q_TEST} ${TEST_QSUB_OPTS} -cwd ${SOURCE_DIR}/client_job.sh $CLIENT_ID $JOB_ARGS
             if [ $? -eq 1 ]; then
                 echo "Submitting to all.q. TEST q is busy."
                 qsub -clear ${Q_ALL} ${QSUB_OPTS} -cwd ${SOURCE_DIR}/client_job.sh $CLIENT_ID $JOB_ARGS
                 ALL_C=$((ALL_C+1))
             else
                 TEST_C=$((TEST_C+1))
             fi
         else
                 echo "SUbmitting to all.q. (Submitted Max to IIHG & UI q.)"
                 qsub -clear ${Q_ALL} ${QSUB_OPTS} -cwd ${SOURCE_DIR}/client_job.sh $CLIENT_ID $JOB_ARGS
                 ALL_C=$((ALL_C+1))
         fi
    fi
    popd 
    if [ $c -ne ${CLIENTS} ]
    then
         sleep ${WAIT_TIME}
    fi
done

# Now monitor until they are all completed.
while(true)
do
    # track the number in setup.
    ls -lat ${TEST_DIR}/${PREFIX}*.setup 2>/dev/null | wc -l | xargs -I {} echo "`date +%s` {}"  >> ${TEST_DIR}/setup.log         
    # track the number running over time
    ls -lat ${TEST_DIR}/${PREFIX}*.running 2>/dev/null | wc -l | xargs -I {} echo "`date +%s` {}" >> ${TEST_DIR}/running.log
    # track the number in cleanup over time
    ls -lat ${TEST_DIR}/${PREFIX}*.cleanup 2>/dev/null | wc -l | xargs -I {} echo "`date +%s` {}" >> ${TEST_DIR}/cleanup.log

    COMPLETED=0
    if [ -f ${TEST_DIR}/completion.log ]
    then
         COMPLETED=`wc -l ${TEST_DIR}/completion.log|cut -d ' ' -f 1`
    fi
    if [ ${COMPLETED} -eq ${CLIENTS} ]
    then
         echo "all done."
         break 
    fi
    sleep 10
done

for i in "${NETSTAT_PID[@]}"
do
   kill -9 ${i}
done
for i in {0..3}
do
   cp ~/performance_test/OSS-0-${i}_dstat.cvs ${TEST_DIR}/OSS-0-${i}_${TEST_NAME}_dstat.csv
done

exit 0
