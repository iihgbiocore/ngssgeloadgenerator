#!/bin/bash

function netstat_sum  {

   if [ `wc -l netstat.tmp | sed 's/  */ /g;s/^ //g' | cut -d " " -f 1` -gt 0 ]
   then
       cat netstat.tmp | sed 's/  */ /g'  >> $2
       RECV_Q=-1
       RECV_Q=`cat netstat.tmp |sed 's/  */ /g' | cut -d " " -f 3 | awk '{ sum+=$1} END {print sum}'`
       SEND_Q=-1
       SEND_Q=`cat netstat.tmp |sed 's/  */ /g' | cut -d " " -f 4 | awk '{ sum+=$1} END {print sum}'`

       echo "$1 $RECV_Q $SEND_Q" >> $3
   else
       echo "$1 0 0" >> $3
   fi
}

SOURCE_DIR=$( cd $( dirname $0 ) && pwd )
cd ${SOURCE_DIR}

if [ $# -lt 1 ]
then
    echo "Must specify test name."
fi

TEST_NAME=$1


FILTER="ftps"
if [ $# -gt 1 ]
then
    FILTER=$2
fi

# Files to write out
HOSTNAME=`hostname`
HOSTNAME=`basename ${HOSTNAME} .local`
NETSTAT_FILE=${HOSTNAME}_netstat-${TEST_NAME}.out
NETSTAT_SUMMARY=${HOSTNAME}_netstat_summary-${TEST_NAME}.out

rm -rf ${NETSTAT_FILE}
rm -rf ${NETSTAT_SUMMARY}

# Set up headers in the files (and replace any file that might already be there.
echo "epoch sum_RecvQ sum_SendQ" > $NETSTAT_SUMMARY
netstat -t | head -n 2 | grep Recv | sed 's/  */ /g' |xargs -I {} echo "epoch {}" > $NETSTAT_FILE


while (true)
do
   NETSTAT_DATE=`date +%s`
   NETSTAT_OUT=`netstat -t | grep "$FILTER" | xargs -I {} echo "$NETSTAT_DATE {}" > netstat.tmp`
   netstat_sum $NETSTAT_DATE $NETSTAT_FILE $NETSTAT_SUMMARY
   sleep 30
done

exit
