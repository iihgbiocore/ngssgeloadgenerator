#!/bin/bash

function netstat_sum  {

   if [ `wc -l netstat.tmp | sed 's/  */ /g;s/^ //g' | cut -d " " -f 1` -gt 0 ]
   then
       cat netstat.tmp | sed 's/  */ /g'  >> $2
       RECV_Q=-1
       RECV_Q=`cat netstat.tmp |sed 's/  */ /g' | cut -d " " -f 3 | awk '{ sum+=$1} END {print sum}'`
       SEND_Q=-1
       SEND_Q=`cat netstat.tmp |sed 's/  */ /g' | cut -d " " -f 4 | awk '{ sum+=$1} END {print sum}'`

       echo "$1 $RECV_Q $SEND_Q" >> $3
   else
       echo "$1 0 0" >> $3
   fi

}

if [ $# -lt 1 ]
then
    echo "Must specify client id."
fi

CLIENT_ID=$1

if [ $# -lt 2 ]
then
    echo "Must specify the directory to store the results in."
    exit 1
fi

DIR=$2

if [ ! -d $DIR ]
then
   mkdir $DIR
fi

pushd $DIR

FILTER="galaxy-storage"
if [ $# -gt 2 ]
then
    FILTER=$3
fi

# Files to write out
NETSTAT_FILE=netstat_$HOSTNAME_${JOB_ID}_${PREFIX}-${CLIENT_ID}.out
NETSTAT_SUMMARY=netstat_summary_$HOSTNAME_${JOB_ID}_${PREFIX}-${CLIENT_ID}.out

# Set up headers in the files (and replace any file that might already be there.
echo "epoch sum_RecvQ sum_SendQ" > $NETSTAT_SUMMARY
netstat -t | head -n 2 | grep Recv | xargs -I {} echo "epoch {}" > $NETSTAT_FILE 


while (true)
do
   NETSTAT_DATE=`date +%s` 
   NETSTAT_OUT=`netstat -t | grep "$FILTER" | xargs -I {} echo "$NETSTAT_DATE {}" > netstat.tmp`
   netstat_sum $NETSTAT_DATE $NETSTAT_FILE $NETSTAT_SUMMARY
   sleep 30
done

popd
exit

