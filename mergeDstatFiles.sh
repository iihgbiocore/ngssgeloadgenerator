#!/bin/bash

TEST_DIR="./"
if [ $# -gt 0 ]
then
   TEST_DIR=$1
fi

rm -rf $TEST_DIR/dstat_summary.*
rm -rf $TEST_DIR/dstat_summary_sorted.*

for f in `find $TEST_DIR -type f -name "dstat_[0-9]**"`
do
    echo $f
    file=`basename ${f} .out`
    arr=(${file//_/ })

    JOB_ID=${arr[1]}
    CLIENT_ID=${arr[2]}

    # Don't want to process the header.
    tail -n+8 ${f}|xargs -I {} echo "$CLIENT_ID,$JOB_ID,{}" >> ${TEST_DIR}/dstat_summary.out
     
done

HEADER='"epoch","client_id","job_id","time","usr","sys","idl","wai","hiq","siq","disk-read","disk-writ","recv","send","io-in","io-out","int","csw","read","writ","used","buff","cach","free"'

# lets copy over the header.
echo ${HEADER} | tr ',' ';' > ${TEST_DIR}/dstat_summary_sorted.ssv
# now, sort the file - minus header.
cat ${TEST_DIR}/dstat_summary.out | sort -t ',' -k 3,3 -n  | tr ',' ';' >>${TEST_DIR}/dstat_summary_sorted.ssv
echo "files merged and sorted: ${TEST_DIR}/dstat_summary_sorted.out ${TEST_DIR}/dstat_summary.out"
exit
