#!/bin/bash

###################################
# This script is intended to be
# launched from a parent script
# which would provide the following 
# variable initialization
#
# DATA_ROOT : where files should be read/written from during the test  -- this should be the suspected bottleneck
# RUN_ROOT : the root directory to consolidate all test results under
# TEST_NAME : the name of the overall test being run (way to group common results from different compute nodes)
# CLIENT_ID : the id of the client being launched
# PREFIX : this is some sub-identifier or name which is used in generating metadata files about the test run.
###################################

###################################
# Helper Functions
###################################
setup_dstat(){
   tar -xjf ${RUN_ROOT}/dstat-0.7.2.tar.bz2 -C /localscratch
   PATH=/localscratch/dstat-0.7.2:$PATH 
   export PATH
}

###################################
# END Helper Functions
###################################

# Set up generic directorys, etc.

# First get the client ID.
CLIENT_ID=$1 
shift

LOCAL_ROOT=/localscratch/$TEST_NAME
TMP_DIR=${LOCAL_ROOT}/${CLIENT_ID}_tmp
LOCAL_DIR=output

START_SETUP=`date +%s`
OUTPUT_DIR=$TEST_NAME
RUN_FILE=${PREFIX}-${CLIENT_ID}_${JOB_ID}

if [ ! -d ${LOCAL_ROOT} ]
then
    mkdir ${LOCAL_ROOT}
fi
if [ ! -d ${LOCAL_ROOT}/${LOCAL_DIR} ]
then
    mkdir ${LOCAL_ROOT}/${LOCAL_DIR}
fi
if [ ! -d ${TMP_DIR} ]
then
    mkdir ${TMP_DIR}
fi

#Kick off monitoring before the run
dstat --list >/dev/null && FOUND=true || setup_dstat
dstat --list >/dev/null && FOUND=true || exit 1

DSTAT_FILE=dstat_$HOSTNAME_${JOB_ID}_${PREFIX}-${CLIENT_ID}.out
if [ -f ${DSTAT_FILE} ]
then
   rm -f ${DSTAT_FILE}
fi

dstat -Ttcdngyrm --output ${LOCAL_ROOT}/${LOCAL_DIR}/${DSTAT_FILE} 30 >/dev/null &
DSTAT_PID=$!
${LG_HOME}/netstat_monitor.sh ${CLIENT_ID} ${LOCAL_ROOT}/${LOCAL_DIR} galaxy-storage >/dev/null &
NETSTAT_PID=$!

find . -type f -iname \'${RUN_FILE}.*\' |xargs -I {} rm -rf {}

echo ${START_SETUP} >> ${RUN_ROOT}/${TEST_NAME}/${RUN_FILE}.setup


if [ ! -d ${DATA_ROOT}/${OUTPUT_DIR} ]
then
    mkdir ${DATA_ROOT}/${OUTPUT_DIR}
fi





###################################
# custom setup
###################################

if [ -f ${LG_HOME}/jobs/${PREFIX}_setup.sh ]
then
    . ${LG_HOME}/jobs/${PREFIX}_setup.sh
fi 

###################################
# custom setup END
###################################


END_SETUP=`date +%s`
echo ${END_SETUP} >> ${RUN_ROOT}/${TEST_NAME}/${RUN_FILE}.setup

SETUP_TIME=`echo $[ ${END_SETUP} - ${START_SETUP} ]`

mv ${RUN_ROOT}/${TEST_NAME}/${RUN_FILE}.setup ${RUN_ROOT}/${TEST_NAME}/`basename ${RUN_FILE}`.setup.${SETUP_TIME}
START_TIME=`date +%s`
echo ${START_TIME} >> ${RUN_ROOT}/${TEST_NAME}/${RUN_FILE}.running

###################################
# run
###################################

. ${LG_HOME}/jobs/${PREFIX}_run.sh

###################################
# run END
###################################

#Run finished, capture monitoring for post analysis

END_TIME=`date +%s`
echo ${END_TIME} >> ${RUN_ROOT}/${TEST_NAME}/${RUN_FILE}.running
RUN_TIME=`echo $[ ${END_TIME} - ${START_TIME} ]`
echo "${PREFIX}_${CLIENT_ID} ${JOB_ID} ${RUN_TIME}" >> ${RUN_ROOT}/${TEST_NAME}/completion.log
mv ${RUN_ROOT}/${TEST_NAME}/${RUN_FILE}.running ${RUN_ROOT}/${TEST_NAME}/`basename ${RUN_FILE}`.running.${RUN_TIME}


START_CLEANUP=`date +%s`
echo ${START_CLEANUP} >> ${RUN_ROOT}/${TEST_NAME}/${RUN_FILE}.cleanup


###################################
# custom cleanup
###################################

if [ -f ${LG_HOME}/jobs/${PREFIX}_cleanup.sh ]
then
    . ${LG_HOME}/jobs/${PREFIX}_cleanup.sh
fi 

###################################
# custom cleanup END
###################################

kill -9 $DSTAT_PID
kill -9 $NETSTAT_PID
#Move all the output files to the run root
mv ${LOCAL_ROOT}/${LOCAL_DIR}/${DSTAT_FILE} ${RUN_ROOT}/${TEST_NAME}/${DSTAT_FILE}
mv ${LOCAL_ROOT}/${LOCAL_DIR}/netstat* ${RUN_ROOT}/${TEST_NAME}/.

#Move all tmp output. 
mv $TMP_DIR ${RUN_ROOT}/${TEST_NAME}/. 

rm -rf ${LOCAL_ROOT}

END_CLEANUP=`date +%s`
echo ${END_CLEANUP} >> ${RUN_ROOT}/${TEST_NAME}/${RUN_FILE}.cleanup
CLEANUP_TIME=`echo $[ ${END_CLEANUP} - ${START_CLEANUP} ]`

mv ${RUN_ROOT}/${TEST_NAME}/${RUN_FILE}.cleanup ${RUN_ROOT}/${TEST_NAME}/`basename ${RUN_FILE}`.cleanup.${CLEANUP_TIME}

exit
