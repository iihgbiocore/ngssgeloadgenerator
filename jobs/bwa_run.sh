#!/bin/bash

###################################
# run
###################################

python ${LG_HOME}/jobs/bwa_wrapper.py        --threads="16"              --illumina1.3        --fileSource=indexed         --ref=$REF        --do_not_build_index        --input1=$INPUT_FILE1       --input2=$INPUT_FILE2  --output=$OUTPUT_FILE        --genAlignType=paired       --params=full         --maxEditDist=3         --fracMissingAligns=0.04         --maxGapOpens=1         --maxGapExtens=-1         --disallowLongDel=16         --disallowIndel=5         --seed=-1   --maxEditDistSeed=2         --mismatchPenalty=3         --gapOpenPenalty=11         --gapExtensPenalty=4         --suboptAlign=false         --noIterSearch=false         --outputTopN=3         --outputTopNDisc=10         --maxInsertSize=500         --maxOccurPairing=100000           --rgid="exome_acatcg_170"           --rgcn=""           --rgds=""           --rgdt=""           --rgfo=""         --rgks=""           --rglb="exome_acatcg_170"           --rgpg=""           --rgpi=""           --rgpl="ILLUMINA"           --rgpu=""           --rgsm="exome_acatcg_170"        --suppressHeader=false  --tmp_dir=$TMP_DIR

###################################
# run END
###################################

