#!/bin/bash

###################################
# custom setup
###################################

if [ $# -lt 2 ]
then
        echo "ERROR: Specify forward & reverse fastqs $#. $@"
        exit 1
fi

# Set up output directories for the test run
if [ ! -d ${DATA_ROOT}/${OUTPUT_DIR} ]
then
      mkdir ${DATA_ROOT}/${OUTPUT_DIR}
fi


IN1=$1
IN2=$2

LOCAL_REF=false
if [ $# -gt 2 ]
then
     LREF=$3
     LOCAL_REF=true
fi


OUTPUT_FILE=${DATA_ROOT}/${OUTPUT_DIR}/${PREFIX}-${CLIENT_ID}.output.sam
if [ -f ${OUTPUT_FILE} ]
then
   # remove the file, this job could be been restarted due to checkpointing.
   rm -rf ${OUTPUT_FILE}
fi

REF=/mnt/radon/galaxy/NGS/indices/hg19_gatk

# run all dependent files off of local disk
if $LOCAL_REF
then
     if [ ! -d /localscratch/bwa_hg19_plus_random.${CLIENT_ID} ]
     then
         mkdir /localscratch/bwa_hg19_plus_random.${CLIENT_ID}
     fi
     cp ${LREF} /localscratch/`basename ${LREF}`.${CLIENT_ID}
     tar -xzvf /localscratch/`basename ${LREF}`.${CLIENT_ID} -C /localscratch/bwa_hg19_plus_random.${CLIENT_ID}
     REF=/localscratch/bwa_hg19_plus_random.${CLIENT_ID}/hg19_gatk
fi


#Copy all files to local location

cp ${IN1}.${CLIENT_ID} ${LOCAL_ROOT}/${LOCAL_DIR}/`basename ${IN1}`
cp ${IN2}.${CLIENT_ID} ${LOCAL_ROOT}/${LOCAL_DIR}/`basename ${IN2}`


#INPUT_FILE1=${LOCAL_ROOT}/${LOCAL_DIR}/`basename ${IN1}`
#INPUT_FILE2=${LOCAL_ROOT}/${LOCAL_DIR}/`basename ${IN2}`

INPUT_FILE1=${IN1}.${CLIENT_ID}
INPUT_FILE2=${IN2}.${CLIENT_ID}


###################################
# custom setup END
###################################
