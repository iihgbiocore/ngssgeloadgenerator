#!/bin/bash

###################################
# custom setup
###################################

if [ $# -lt 4 ]
then
        echo "ERROR: Specify forward & reverse fastqs and the bed file to use. $#. $@"
        exit 1
fi

# Set up output directories for the test run
if [ ! -d ${DATA_ROOT}/${OUTPUT_DIR} ]
then
      mkdir ${DATA_ROOT}/${OUTPUT_DIR}
fi

#REF=/mnt/radon/galaxy/NGS/indices/hg19_gatk
REF=/iihg/data/NGS/indices/bwa_hg19_plus_random/hg19_gatk
IN1=""
INPUT_FILE1=$IN1
IN2=""
INPUT_FILE2=$IN2
ALN1=""
ALN_FILE1=$ALN1
ALN2=""
ALN_FILE2=$ALN2
LOCAL_REF=false
COPY_OUT=false
INPUT_READ="local"
OUTPUT_FILE=""
OUTPUT_FINAL=$OUTPUT_FILE

usage(){

cat << EOF

usage: $0 options

This script will setup input and output files for bwa sampe step

OPTIONS:
   -f      forward fastq
   -r      reverse fastq
   -a      forward aln
   -b      reverse aln
   -l      local reference indices - archive to extract locally
   -i      reference indice files to use.
   -c      write the output file locally, then copy to final storgae location
   -s      local,remote,split.  How to feed the input files into bwa-sampe - either all local, all remote, or split them between local/remote (aln will be local).  

EOF

}

printArgs(){
cat << EOF

run arguments:

     REF = ${REF}
     IN1 = ${IN1}
     INPUT_FILE1 = ${INPUT_FILE1}
     IN2 = ${IN2}
     INPUT_FILE2 = ${INPUT_FILE2}
     ALN1 = ${ALN1}
     ALN_FILE1 = ${ALN_FILE1}
     ALN2 = ${ALN2}
     ALN_FILE2 = ${ALN_FILE2}
     INPUT_READ = ${INPUT_READ}
     OUTPUT_FILE = ${OUTPUT_FILE}
     OUTPUT_FINAL = ${OUTPUT_FINAL}
     COPY_OUT = ${COPY_OUT}
EOF
}

while getopts "f:r:a:b:l:i:cs:" OPTION
do
     case $OPTION in
         f)
             IN1=$OPTARG
             ;;
         r)
             IN2=$OPTARG
             ;;
         a)
             ALN1=$OPTARG
             ;;
         b)
             ALN2=$OPTARG
             ;;
         l)
             LOCAL_REF=true
             LREF=$OPTARG
             ;;
         i)
             REF=$OPTARG
             ;;
         c)
             COPY_OUT=true
             ;;
         s)
             INPUT_READ=$OPTARG
             INPUT_READ=$(echo $INPUT_READ | tr "[:upper:]" "[:lower:]")
             ;;
         ?)
             usage
             exit
             ;;
     esac
done


OUTPUT_FILE=${DATA_ROOT}/${OUTPUT_DIR}/${PREFIX}-${CLIENT_ID}.output.sam
OUTPUT_FINAL=${OUTPUT_FILE}
if $COPY_OUT
then
    OUTPUT_FILE=${LOCAL_ROOT}/${LOCAL_DIR}/${PREFIX}-${CLIENT_ID}.output.sam
fi


if [ -f ${OUTPUT_FILE} ]
then
   # remove the file, this job could be been restarted due to checkpointing.
   rm -rf ${OUTPUT_FILE}
fi


# run all dependent files off of local disk
if $LOCAL_REF
then
     if [ ! -d /localscratch/bwa_hg19_plus_random.${CLIENT_ID} ]
     then     
         mkdir /localscratch/bwa_hg19_plus_random.${CLIENT_ID}
     fi
     cp ${LREF} /localscratch/`basename ${LREF}`.${CLIENT_ID}
     tar -xzvf /localscratch/`basename ${LREF}`.${CLIENT_ID} -C /localscratch/bwa_hg19_plus_random.${CLIENT_ID}
     REF=/localscratch/bwa_hg19_plus_random.${CLIENT_ID}/hg19_gatk
fi


#Copy all files to local location

if [ "${INPUT_READ}" != "local" ]
then
    INPUT_FILE1=${IN1}.${CLIENT_ID}
    INPUT_FILE2=${IN2}.${CLIENT_ID}
else
    cp ${IN1}.${CLIENT_ID} ${LOCAL_ROOT}/${LOCAL_DIR}/`basename ${IN1}`
    cp ${IN2}.${CLIENT_ID} ${LOCAL_ROOT}/${LOCAL_DIR}/`basename ${IN2}`
    INPUT_FILE1=${LOCAL_ROOT}/${LOCAL_DIR}/`basename ${IN1}`
    INPUT_FILE2=${LOCAL_ROOT}/${LOCAL_DIR}/`basename ${IN2}`
fi


if [ "${INPUT_READ}" == "remote" ]
then
   ALN_FILE1=${ALN1}.${CLIENT_ID}
   ALN_FILE2=${ALN2}.${CLIENT_ID}
else
   cp ${ALN1}.${CLIENT_ID} ${LOCAL_ROOT}/${LOCAL_DIR}/`basename ${ALN1}`
   cp ${ALN2}.${CLIENT_ID} ${LOCAL_ROOT}/${LOCAL_DIR}/`basename ${ALN2}`
   ALN_FILE1=${LOCAL_ROOT}/${LOCAL_DIR}/`basename ${ALN1}`
   ALN_FILE2=${LOCAL_ROOT}/${LOCAL_DIR}/`basename ${ALN2}`
fi

printArgs

###################################
# custom setup END
###################################
