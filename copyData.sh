#!/bin/bash

pushd /mnt/radon/galaxy/performance_test/reference_files

for i in {1..91}
do
#    if [ ! -f WE-D1-1.fastq.${i} ]
#    then
         cp WE-D1-1.fastq WE-D1-1.fastq.${i}
         cp WE-D1-2.fastq WE-D1-2.fastq.${i}
         cp WE-D1-2.align.out WE-D1-2.align.out.${i}
         cp WE-D1-1.align.out WE-D1-1.align.out.${i}
#    fi
done

popd

exit
