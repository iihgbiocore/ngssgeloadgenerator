#!/bin/bash

PATTERN=""
if [ $# -gt 0 ]
then
   PATTERN=$1
fi

rm -rf ${PATTERN}_netstat_*
rm -rf ${PATTERN}_dstat_*
rm -rf ${PATTERN}_sge_*
rm -rf ${PATTERN}_gs_*


HEADER_OUT=true

for d in `find . -maxdepth 1 -type d -name "*${PATTERN}*"`
do
    echo $d

    TEST_NAME=`basename ${d}`

#    ./mergeNetstatFiles.sh ${TEST_NAME} 
#    ./mergeDstatFiles.sh ${TEST_NAME}
#    ./getSGEaccnt_stats.sh ${TEST_NAME}

    if [ ! -f ${d}/netstat_summary_sorted.ssv ]
    then
       echo "${TEST_NAME}: merging netstat output"
       ./mergeNetstatFiles.sh ${TEST_NAME} 
    fi

    if [ ! -f ${d}/dstat_summary_sorted.ssv ]
    then
       echo "${TEST_NAME}: merging dstat output"
      ./mergeDstatFiles.sh ${TEST_NAME}
    fi


    if [ ! -f ${d}/sge_accnt.ssv ]
    then
       echo "${TEST_NAME}: getting sge accounting info"
      ./getSGEaccnt_stats.sh ${TEST_NAME}
    fi
    
    pushd ${d}  > /dev/null
     
    echo "${TEST_NAME}: rolling up all data"  
    if $HEADER_OUT
    then
        TYPES="string;string;string;integer;integer;integer"
        HEADER=`head -n 1 netstat_summary_sorted.ssv` 
        echo "group;${HEADER}" | tr " " ";" > ../${PATTERN}_netstat_summary.ssv
        echo ${TYPES} >> ../${PATTERN}_netstat_summary.ssv

        HEADER=`head -n 1 dstat_summary_sorted.ssv`
        TYPES="string;string;integer;integer;date;decimal;decimal;decimal;decimal;decimal;decimal;decimal;decimal;decimal;decimal;decimal;decimal;decimal;decimal;decimal;decimal;decimal;decimal;decimal;decimal"
        echo "group;${HEADER}" > ../${PATTERN}_dstat_summary.ssv
        echo "${TYPES}" >> ../${PATTERN}_dstat_summary.ssv

        HEADER=`head -n 1 sge_accnt.ssv`
        TYPES="string;string;string;string;string;string;integer;string;integer;integer;integer;integer;integer;integer;decimal;decimal;decimal;decimal;integer;integer;integer;integer;integer;integer;integer;decimal;integer;integer;integer;integer;integer;integer;string;string;string;integer;integer;decimal;decimal;decimal;string;decimal;string;decimal;integer;integer;integer;integer;integer"
        echo "tgroup;${HEADER}" > ../${PATTERN}_sge_accnt.ssv
        echo "${TYPES}" >> ../${PATTERN}_sge_accnt.ssv

        HEADER="epoch;time;%usr;%sys;%wio;%idle"
        TYPES="string;integer;integer;integer"
        echo "group;${HEADER}" > ../${PATTERN}_gs_sarstat_summary.ssv 
        echo "${TYPES}" >> ../${PATTERN}_gs_sarstat_summary.ssv 

        HEADER="epoch;sum_RecvQ;sum_SendQ"
        TYPES="string;string;string;integer;integer;integer;integer"
        echo "group;${HEADER}" > ../${PATTERN}_gs_netstat_summary.ssv 
        echo "${TYPES}" >> ../${PATTERN}_gs_netstat_summary.ssv 
    
        HEADER_OUT=false
    fi

    tail -n+2 netstat_summary_sorted.ssv | xargs -I {} echo "${TEST_NAME};{}" >> ../${PATTERN}_netstat_summary.ssv
    tail -n+2 dstat_summary_sorted.ssv | xargs -I {} echo "${TEST_NAME};{}" >> ../${PATTERN}_dstat_summary.ssv
    tail -n+2 sge_accnt.ssv | xargs -I {} echo "${TEST_NAME};{}" >> ../${PATTERN}_sge_accnt.ssv

    tail -n+2 galaxy-storage_netstat_summary-${TEST_NAME}.out | xargs -I {} echo "${TEST_NAME} {}" | tr " " ";" >>../${PATTERN}_gs_netstat_summary.ssv 
    tail -n+3 galaxy-storage_sarstat_${TEST_NAME}.out | xargs -I {} echo "${TEST_NAME} {}" | sed 's/  */ /g' | tr " " ";" >>../${PATTERN}_gs_sarstat_summary.ssv 

    popd  > /dev/null

done

# SORT!

echo "Sorting all rolled up data"

# lets copy over the header.
head -n 2 ${PATTERN}_netstat_summary.ssv > ${PATTERN}_netstat_summary_sorted.ssv
head -n 2 ${PATTERN}_dstat_summary.ssv > ${PATTERN}_dstat_summary_sorted.ssv

# now, sort the file - minus header.
tail -n+3 ${PATTERN}_netstat_summary.ssv | sort -t ' ' -k 4,4 -n >> ${PATTERN}_netstat_summary_sorted.ssv
tail -n+3 ${PATTERN}_dstat_summary.ssv | sort -t ' ' -k 4,4 -n >> ${PATTERN}_dstat_summary_sorted.ssv

# all done.
echo "files merged and sorted: ${PATTERN}_netstat_summary_sorted.ssv ${PATTERN}_netstat_summary.ssv"
echo "files merged and sorted: ${PATTERN}_dstat_summary_sorted.ssv ${PATTERN}_dstat_summary.ssv"
echo "files merged: ${PATTERN}_sge_accnt.ssv"

exit
