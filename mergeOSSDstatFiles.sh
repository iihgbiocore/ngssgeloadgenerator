#!/bin/bash

PATTERN=""
if [ $# -gt 0 ]
then
   PATTERN=$1
fi

rm -rf ${PATTERN}_OSS_dstat_*


HEADER_OUT=true

for d in `find . -maxdepth 1 -type d -name "*${PATTERN}*gluster*"`
do
    if $HEADER_OUT
    then
       echo '"group","server","epoch","time","usr","sys","idl","wai","hiq","siq","1m","5m","15m","dsk-read","dsk-writ","recv","send","in","out","int","csw","io-read","io-writ","used","buff","cach","free"' |tr "," ";" >> ${PATTERN}_OSS_dstat_summary.ssv
       echo "string,string,integer,string,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal" | tr "," ";" >> ${PATTERN}_OSS_dstat_summary.ssv
       HEADER_OUT=false
    fi
    TEST_NAME=`basename ${d}`
    # Don't want to process the header.
    for i in {0..3}
    do
       if [ -f "${d}/OSS-0-${i}_${TEST_NAME}_dstat.csv" ]
       then
           FILESIZE=$(stat -c%s "${d}/OSS-0-${i}_${TEST_NAME}_dstat.csv")
           if [ "${FILESIZE}" -gt "100" ]
           then
               # At some point we started to include load stats for the OSS servers.  Need to normalize to put 0 place holders in for files that did not have load metrics
               FIELDS=`tail -n1 ${d}/OSS-0-${i}_${TEST_NAME}_dstat.csv | awk -F"," '{print NF}'`
               if [ "$FIELDS" -eq "22" ]
               then
                    tail -n+8 ${d}/OSS-0-${i}_${TEST_NAME}_dstat.csv | awk -F "\"*,\"*" '{print $1","$2","$3","$4","$5","$6","$7","$8",0,0,0,"$9","$10","$11","$12","$13","$14","$15","$16","$17","$18","$19","$20","$21","$22}' | xargs -I {} echo "${TEST_NAME},OSS-0-${i},{}" |  tr "," ";" >> ${PATTERN}_OSS_dstat_summary.ssv
                    # awk -F "\"*,\"*" '{print $1,$2,$3,$4,$5,$6,$7,$8,0,0,0,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22}' ${d}/OSS-0-${i}_${TEST_NAME}_dstat.csv | xargs -I {} echo "${TEST_NAME},OSS-0-${i},{}" |  tr "," ";" >> ${PATTERN}_OSS_dstat_summary.ssv  
               else           
                    tail -n+8 ${d}/OSS-0-${i}_${TEST_NAME}_dstat.csv | xargs -I {} echo "${TEST_NAME},OSS-0-${i},{}" |  tr "," ";" >> ${PATTERN}_OSS_dstat_summary.ssv    
               fi
           fi
       else
           echo "${d}/OSS-0-${i}_${TEST_NAME}_dstat.csv does not exist."
       fi 
    done
     
done

exit
