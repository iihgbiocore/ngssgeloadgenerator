#!/bin/bash

TEST_DIR="./"
if [ $# -gt 0 ]
then
   TEST_DIR=$1
fi

function sgelookup {
   JOB_IDS=$1
   IFS=$'\n'
   ACCNT_LINES=( $(egrep "^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:(${JOB_IDS})" $SGE_ROOT/$SGE_CELL/common/accounting) )
   for line in ${ACCNT_LINES[@]}
   do
      JOB_ID=`echo "${line}" | cut -d ":" -f 6`
      RUN_FILE=`find $TEST_DIR -maxdepth 1 -type f -name "*_${JOB_ID}.running.*"`
      RUN_FILE=`basename ${RUN_FILE}`
      SETUP_FILE=`find $TEST_DIR -maxdepth 1 -type f -name "*_${JOB_ID}.setup.*"`
      SETUP_FILE=`basename ${SETUP_FILE}`
      CLEANUP_FILE=`find $TEST_DIR -maxdepth 1 -type f -name "*_${JOB_ID}.cleanup.*"`
      CLEANUP_FILE=`basename ${CLEANUP_FILE}`

      RUN_TIME=${RUN_FILE##*.}
      SETUP_TIME=${SETUP_FILE##*.}
      CLEAN_TIME=${CLEANUP_FILE##*.}

      echo $line:$RUN_TIME:$SETUP_TIME:$CLEAN_TIME >> $TEST_DIR/sge_accnt.log
  done
}

rm -rf $TEST_DIR/sge_accnt.*

# Insert the header
echo "qname:hostname:group:owner:jobname:jobnumber:account:priority:qsub_time:start_time:end_time:failed:exit_status:ru_wallclock:ru_utime:ru_stime:ru_maxrss:ru_ixrss:ru_ismrss:ru_idrss:ru_isrss:ru_minflt:ru_majflt:ru_nswap:ru_inblock:ru_oublock:ru_msgsnd:ru_msgrcv:ru_nsignals:ru_nvcsw:ru_nivcsw:project:department:granted_pe:slots:task_num:cpu:mem:io:qsub_args:iow:taskid:maxvmem:arid:artime:run_time:setup_time:cleanup_time" > $TEST_DIR/sge_accnt.log

#cat $TEST_DIR/completion.log | cut -d " " -f 2 | xargs -I {} grep ".*:.*:.*:.*:.*:{}" $SGE_ROOT/$SGE_CELL/common/accounting >> $TEST_DIR/sge_accnt.log
#cat $TEST_DIR/completion.log | cut -d " " -f 2 | xargs -I {} ./sgelookup.sh {} >> $TEST_DIR/sge_accnt.log
#cat $TEST_DIR/completion.log | cut -d " " -f 2 >> $TEST_DIR/sge_accnt_tmp.log

JOBIDS=""
FIRST=true
for line in $(cat $TEST_DIR/completion.log | cut -d " " -f 2)
do
   if $FIRST
   then
      JOBIDS=$line
      FIRST=false
   else
      JOBIDS="$JOBIDS|$line"
   fi
done

if [ "$JOBIDS" == "" ]
then
   echo "JOBIDS was empty - aborting!"
   exit
fi

sgelookup $JOBIDS 

#Now lets convert ':' to ';'
cat $TEST_DIR/sge_accnt.log | tr ":" ";" >> $TEST_DIR/sge_accnt.ssv

exit
